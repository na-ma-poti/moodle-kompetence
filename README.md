# Kompetenčna ogrodja za naravoslovno in matematično pismenost

V tem repotzitoriju so na voljo [kompetenčna ogrodja](https://docs.moodle.org/en/Competency_frameworks) za učno platformo [Moodle](https://moodle.org/), ki so nastala v okviru projekta [NAMA-Poti](https://www.zrss.si/projekti/projekt-na-ma-poti/).

## Namestitev kompetenc

Namestitev kompetenčnega ogrodja lahko opravi administrator učne platforme Moodle. Prenesite izbrano ogrodje kompetenc

* [ogrodje za matemtatično pismenost](Matematicna_pismenost_NAMA-Poti.csv)
* [ogrodje za naravoslovno pismenost](Naravoslovna_pismenost_NAMA-Poti.csv)

in sledite navodilom za [namestitev kompetenčnega ogrodja v Moodle](https://docs.moodle.org/en/Competency_frameworks).

## Učni načrti

Administrator učne platforme Moodle lahko pripravi osnutek/predlogo za [učne načrte](https://docs.moodle.org/sl/Learning_plans). Predloga za učni načrt je nabor kompetenc, ki jih lahko administrator učne platforme predpiše učencem ali kohortam učencev.

Učenci in učitelji lahko za spremljajo napredek po učnem načrtu. Učenci pridobivajo kompetence, tako da sodelujejo v aktivnostih, ki so potrebne za pridobitev kompetenc v učnem načrtu.
## Uporaba kompetenc

Kompetenčno ogrodje vsebuje hierarhijo kompetenc za matematično in naravoslovno pismenost ([gradniki matematične pismenosti](https://www.zrss.si/wp-content/uploads/2021/05/Matematicna_pismenost.pdf) in [gradniki naravoslovne pismenosti](https://www.zrss.si/wp-content/uploads/2021/05/Gradniki-NP-verzija-10a.pdf)). Na vrhu hierarhije so gradniki, nivo nižje so podgradniki, na naslednjem nivoju so učna obdobja (vrtec, 1. triletje OŠ, ...) in na dnu hierarhije so podrobnejši opisniki posameznih sposobnosti in veščin. 

Učitelji lahko dodajo kompetence predmetom in posameznim aktvnostim znotraj predmeta. Prav tako lahko spremljajo napredek učencev in pregledajo dokaze o že osvojenih kompetencah učencev, ki so jih učenci naložili.

Učenci lahko pregledajo svoj učni načrt, naložijo dokaze o že pridobljenih kompetencah in zahtevajo, da jih učitelji pregledajo. Več o tem si lahko preberete v [pogostih vprašanjih o kompetencah](https://docs.moodle.org/sl/Competencies_FAQ)

Več o gradnikih [matematične](https://www.zrss.si/wp-content/uploads/2021/05/Matematicna_pismenost.pdf) in [naravoslovne pismenosti](https://www.zrss.si/wp-content/uploads/2021/05/Gradniki-NP-verzija-10a.pdf) si lahko preberete na [strani projekta NAMA-Poti](https://www.zrss.si/projekti/projekt-na-ma-poti/).

Gradivo je nastalo v okviru projekta [NAMA-Poti](https://www.zrss.si/projekti/projekt-na-ma-poti/) v sodelovanju:

* [Fakultete za računalništvo in informatiko, Univerze v Ljubljani](https://www.fri.uni-lj.si)
* [Zavoda za šolstvo Republike Slovenije](https://www.zrss.si)
* [![MIZŠ Republike Slovenije](logotip-mizs.png)](https://www.gov.si/drzavni-organi/ministrstva/ministrstvo-za-izobrazevanje-znanost-in-sport/)
* [![Evropski socialni sklad](logo-ess-2016.png)](https://www.eu-skladi.si/)