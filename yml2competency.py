import yaml
import csv
import sys
import os

def opisnik_line(opisnik, parent_id, count):
    abc = 'abcdefghijklmnoprst'
    id = "{}-{}".format(parent_id, abc[count])
    return [parent_id, id, opisnik, opisnik, 2,] + [None for k in range(9)]

def nivo_csv(nivo, parent_id):
    "Generiraj CSV vrstice za nivo"
    opis = nivo['nivo']
    id = "{}-{}".format(parent_id, opis)
    lines = [[ parent_id, id, opis, opis, 2] + [None for k in range(9)]]
    k = 0
    for opisnik in nivo['opisniki']:
        lines.append(opisnik_line(opisnik, id, k))
        k += 1
    return lines

def podgradnik_csv(gradnik, parent_id):
    "Generiraj CSV vrstice za podgradnik"
    id = gradnik['id']
    lines = [[parent_id, id, gradnik['opis'], gradnik['opis'], 2] + [None for k in range(9)]]
    for nivo in gradnik['nivoji']:
        lines += nivo_csv(nivo, id)
    return lines

def gradnik_csv(gradnik, parent_id):
    "Generiraj CSV vrstice za posamezen gradnik"
    id = gradnik['id']
    lines = [[parent_id, id, gradnik['ime'], gradnik['opis'], 2] + [None for k in range(9)]]
    for podgradnik in gradnik['podgradniki']:
        lines += podgradnik_csv(podgradnik, id)
    return lines

def cf_csv(data):
    "Convert data to CSV lines"
    taxonomy = "concept,competency,level,competency"
    header = [
        'Parent ID number',
        'ID number',
        'Short name',
        'Description',
        'Description format',
        'Scale values',
        'Scale configuration',
        'Rule type (optional)',
        'Rule outcome (optional)',
        'Rule config (optional)',
        'Cross-referenced competency ID numbers',
        'Exported ID (optional)',
        'Is framework',
        'Taxonomy']
    scale = "ni dosežena, obvlada"
    scale_config = "[{""scaleid"":""2""},{""id"":2,""scaledefault"":1,""proficient"":1}]"
    id = data['id'] 
    framework_line = [
        None, # 'Parent ID number',
        id,  # 'ID number',
        data['ime'], # 'Short name',
        data['opis'], # 'Description',
        1, # 'Description format',
        scale, # 'Scale values',
        scale_config, # 'Scale configuration',
        None, # 'Rule type (optional)',
        None, # 'Rule outcome (optional)',
        None, # 'Rule config (optional)',
        None, # 'Cross-referenced competency ID numbers',
        None, # 'Exported ID (optional)',
        1, # 'Is framework',
        taxonomy ]# 'Taxonomy']
    lines = [header, framework_line]
    for gradnik in data['gradniki']:
        lines += gradnik_csv(gradnik, id)
    return lines

if __name__ == '__main__':
    #filename = sys.argv[1]
    filename = 'Naravoslovna_pismenost-NAMA-Poti.yml'
    csvpath = "{}.csv".format(os.path.splitext(filename)[0])
    with open(filename) as file:
        data = yaml.load(file, Loader=yaml.FullLoader)
    csv_lines = cf_csv(data)
    with open(csvpath, 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        writer.writerows(csv_lines)